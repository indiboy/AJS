;(function($){
    var u = {}, isAndroid = (/android/gi).test(navigator.appVersion);
    $.extend({
        isElement : function(obj){
            return !!(obj && obj.nodeType == 1);
        },        
        fixIos7Bar : function(el){
            if(!JSLite.isElement(el)){
                console.warn('$api.fixIos7Bar Function need el param, el param must be DOM Element');
                return;
            }
            var strDM = api.systemType;
            if (strDM == 'ios') {
                var strSV = api.systemVersion;
                var numSV = parseInt(strSV,10);
                var fullScreen = api.fullScreen;
                var iOS7StatusBarAppearance = api.iOS7StatusBarAppearance;
                if (numSV >= 7 && !fullScreen && iOS7StatusBarAppearance) {
                    el.style.paddingTop = '20px';
                }
            }
        },
        fixStatusBar : function(el){
            if(!JSLite.isElement(el)){
                console.warn('$api.fixStatusBar Function need el param, el param must be DOM Element');
                return;
            }
            var sysType = api.systemType;
            if(sysType == 'ios'){
                JSLite.fixIos7Bar(el);
            }else if(sysType == 'android'){
                var ver = api.systemVersion;
                ver = parseFloat(ver);
                if(ver >= 4.4){
                    el.style.paddingTop = '25px';
                }
            }
        },
        isPhoneNumber:function(tel){
             var reg = /^0?1[3|4|5|8][0-9]\d{8}$/;
             return reg.test(tel)
        },
        uzStorage : function(){
            var ls = window.localStorage;
            if(isAndroid){
               ls = os.localStorage();
            }
            return ls;
        },
        setStorage : function(key, value){
            if(arguments.length === 2){
                var v = value;
                if(typeof v == 'object'){
                    v = JSON.stringify(v);
                    v = 'obj-'+ v;
                }else{
                    v = 'str-'+ v;
                }
                var ls = JSLite.uzStorage();
                if(ls){
                    ls.setItem(key, v);
                }
            }
        },
        getStorage : function(key){
            var ls = JSLite.uzStorage();
            if(ls){
                var v = ls.getItem(key);
                if(!v){return;}
                if(v.indexOf('obj-') === 0){
                    v = v.slice(4);
                    return JSON.parse(v);
                }else if(v.indexOf('str-') === 0){
                    return v.slice(4);
                }
            }
            return "";
        },
        rmStorage : function(key){
            var ls = JSLite.uzStorage();
            if(ls && key){
                ls.removeItem(key);
            }
        },
        clearStorage : function(){
            var ls = JSLite.uzStorage();
            if(ls){
                ls.clear();
            }
        },
        toast : function(title, text, time){
            if(this.isMobile()){
                window.alert(text);
                return false;
            }
            var opts = {};
            var show = function(opts, time){
                api.showProgress(opts);
                setTimeout(function(){
                    api.hideProgress();
                },time);
            };
            if(arguments.length === 1){
                var time = time || 500;
                if(typeof title === 'number'){
                    time = title;
                }else{
                    opts.title = title+'';
                }
                show(opts, time);
            }else if(arguments.length === 2){
                var time = time || 500;
                var text = text;
                if(typeof text === "number"){
                    var tmp = text;
                    time = tmp;
                    text = null;
                }
                if(title){
                    opts.title = title;
                }
                if(text){
                    opts.text = text;
                }
                show(opts, time);
            }
            if(title){
                opts.title = title;
            }
            if(text){
                opts.text = text;
            }
            time = time || 500;
            show(opts, time);
        },
        
        UrlParamToArray:function(url){
            if (!url) url = window.location.href;
            var paraString = url.substring(url.indexOf("?")+1,url.length).split("&");
            var paraObj = {} ;
            var j = 0,i=0;
            for (i=0; j=paraString[i]; i++){
                paraObj[j.substring(0,j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=")+1,j.length);    
            }
            return paraObj;
        },
        openWin:function(url,name){
            if (!$.isMobile()) {
                window.location.href = url;
                return false;
            }
            var parArr = {},
            parArr = JSLite.UrlParamToArray(url);
            var delay = 0;
            if("ios" != api.systemType){
                delay = 300;
            }
            api.openWin({
                name: name,
                url: url,
                pageParam: parArr,
                bounces: false,
                vScrollBarEnabled:true,
                hScrollBarEnabled:false,
                reload: true,
                delay: delay
            });
        },
    });
})(JSLite);