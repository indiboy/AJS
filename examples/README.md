### 实例说明
AJSPM25 意为AJS PM 2.5 查询，可以查询所选择城市的PM2.5值，默认查询的是杭州。    
Android 下载，只有Android版本的。    
![Android 下载][1]
预览   
![应用预览][2]
###目录结构

├── config.xml    ApiCloud的配置文件    
├── cover.png    启动页    
├── css       
│   ├── app.css    
│   └── mui.min.css    [mui][3]    
├── fonts    
├── html    
│   ├── about.html    关于页面    
│   └── city.html    城市选择页面    
├── icon.png    app 图标    
├── img    资源图文   
├── index.html    首页   
├── js    
│   └── ajs.min.js    
└── res    
    └── cityList.json    资源文件   


###代码说明
例子比较简单，包括主页面、介绍页面、城市选择页面共三个页面，所有页面都引用了ajs.js。    
代码解读

    $(function(){
    }）

完全兼容JQ的写法，很顺手，当封装到ApiCloud当中之后等同于

    apiready = function(){
    }
然后是通过ajax获取数据    

    $.get(url,function(req){
    })

当在PC上调试的时候和平时web无异，当在ApiCloud中时候则会调用内置的api.ajax。    
**注意** 不在ApiCloud中时，使用的是jsonp的方式。    

获取窗口参数

    $.getUrlParam('city');
    
在ApiCloud中则等同于 

    api.pageParam('city');
否则就是直接获取url中的参数。代码实现

    getUrlParam:function (name,is) {
            if (JSLite.isMobile() && is != 'web') {
                var pageParam = api.pageParam
                return pageParam[name];
            };
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i"),
            r = window.location.search.substr(1).match(reg);
            if (r != null) return unescape(r[2]); return null;
        },

跳转窗口说明

    $.openWin(url,name);
**url**：打开的html地址，可以带参数，如 index.html?city=hangzhou    
**name**：打开的窗口的名字。    
代码实现

    openWin:function(url,name){
            if (!$.isMobile()) {
                window.location.href = url;
                return false;
            }
            var parArr = {},
            parArr = JSLite.UrlParamToArray(url);
            var delay = 0;
            if("ios" != api.systemType){
                delay = 300;
            }
            api.openWin({
                name: name,
                url: url,
                pageParam: parArr,
                bounces: false,
                vScrollBarEnabled:true,
                hScrollBarEnabled:false,
                reload: true,
                delay: delay
            });
        },

###打包ApiCloud
下载本项目源码，修改 examples/config.xml中你的ApiCloud  ID，将 examples 改名为 widget 然后压缩为zip包即可。

###感谢

 - [ApiCloud][4]
 - [mui][5]
 - [列表项][6]


  [1]: http://git.oschina.net/anyhome/AJS/raw/master/examples/qr.png
  [2]: http://static.oschina.net/uploads/space/2015/0517/004655_aNP4_1423274.png
  [3]: http://dcloudio.github.io/mui/
  [4]: http://www.apicloud.com
  [5]: http://dcloudio.github.io/mui/
  [6]: http://jslite.io