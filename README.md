# AJS

看到被推荐了，我觉得应该把说明文档写更好。

---
###概述
   由于个人实在不习惯ApiCloud官方提供的api端，所以根据自己的喜欢重写了API。    
   AJX是基于[JSLITE][1]重新构建，在原基础上修改了少部分代码，并扩单独扩展了ApiCloud部分。    
   JSLITE是个好东西，代码风格完全兼容JQ，国人开发，小场合下非常适用。    
   
###演示   
这里有关于[ApiCloud][2]的介绍，[演示代码说明][3]。
Android 应用下载。    
![Android应用][4]    
程序预览界面    
![预览][5]

###使用
引入ajs.js
之前的代码风格

    apiready = function(){
        var header = $api.byId('header');
        //兼容iOS7+状态栏
        $api.fixIos7Bar(header); 
    }

现在的代码风格

    $(function(){
        var header = $('header');
        //兼容iOS7+状态栏
        $.fixIos7Bar(header); 
    })
完全兼容JQ的写法，

**AJAX调试**    
ApiCloud官方调试数据获取真是个痛苦的过程，其实调试数据的获取和调试普通web没有什么不同，完全可以在PC上进行。
之前的例子

    $api.get('http://www.pm25.in/api/querys/pm2_5.json?city=beijing&token=5j1znBVAsnSf5xQyNQyq',function(ret){
        alert(ret);
    },'text');

现在的写法

    $.get('http://www.pm25.in/api/querys/pm2_5.json?callback=?&city=beijing&token=5j1znBVAsnSf5xQyNQyq',function(ret){
        alert(ret);
    },'text');

几乎没什么改变，还是按照JQ的写法，通过URL加 callback=?，意思就通过JSONP的方式获取数据，这样就能够远程获取数据了。就能实现本地调试远程数据了。


  [1]: http://jslite.io
  [2]: http://www.apicloud.com
  [3]: http://git.oschina.net/anyhome/AJS/tree/master/examples
  [4]: http://git.oschina.net/anyhome/AJS/raw/master/examples/qr.png
  [5]: http://static.oschina.net/uploads/space/2015/0517/004655_aNP4_1423274.png